%global on_openeuler 1

Name:          kiran-biometrics
Version:       0.0.3
Release:       4
Summary:       Kiran Desktop kiran-biometrics
License:       MulanPSL-2.0
URL:           http://www.kylinsec.com.cn

Source0:       %{name}-%{version}.tar.gz
Patch0:        0001-fix-kiran-biometrics-Modify-the-log-information-leve.patch 
Patch1:        0002-fix-translate-add-some-translation.patch

BuildRequires: glib2-devel
BuildRequires: dbus-glib-devel
%if 0%{?on_openeuler} 
#BuildRequires: fingerprint-sdk-devel
%else
%ifarch x86_64 
BuildRequires: fingerprint-sdk-devel
%endif
%endif
%if 0%{?on_openeuler}
BuildRequires: zlog
%else
BuildRequires: zlog-devel
%endif
BuildRequires: cmake
BuildRequires: make
BuildRequires: pam-devel
BuildRequires: kiran-cc-daemon-devel
#BuildRequires: opencv-glib-devel
#BuildRequires: zeromq-devel
BuildRequires: json-glib-devel
BuildRequires: dbus-glib-devel
BuildRequires: gettext
BuildRequires: gcc-c++

#Requires: facematch

%description
Kiran Biometrics is used do  fprint and face auth for system.

%package devel
Summary:      Development files for kiran biometrics

%description devel
Development files for kiran-biometrics

%prep
%autosetup -p1

%build
%cmake
%cmake_build

%install
%cmake_install
%find_lang %{name}

%files -f %{name}.lang
%{_libexecdir}/kiran_biometrics_manager
%{_sysconfdir}/dbus-1/system.d/kiran_biometrics.conf
%{_datadir}/dbus-1/system-services/com.kylinsec.Kiran.SystemDaemon.Biometrics.service
%{_sysconfdir}/kiran-biometrics/settings.conf
%{_prefix}/lib/systemd/system/kiran-system-daemon-biometrics.service
%if 0%{?on_openeuler}
#%{_libdir}/kiran-fprint-modules/libkiran_arat_fingerprint.so
#%{_libdir}/kiran-fprint-modules/libkiran_zk_fingerprint.so
%else
%ifarch x86_64
%{_libdir}/kiran-fprint-modules/libkiran_arat_fingerprint.so
%{_libdir}/kiran-fprint-modules/libkiran_zk_fingerprint.so
%endif
%endif
%{_libdir}/security/pam_kiran_fprintd.so
%{_libdir}/security/pam_kiran_authmode.so
#%{_libdir}/security/pam_kiran_face.so


%files devel
%{_includedir}/kiran-pam-msg.h
%{_includedir}/kiran-fprint-module.h
%{_includedir}/kiran-biometrics/kiran-biometrics-i.h

%changelog
* Wed Nov 20 2024 Funda Wang <fundawang@yeah.net> - 0.0.3-4
- adopt to new cmake macro

* Mon Apr 10 2023 wangyucheng <wangyucheng@kylinsec.om.cn> - 0.0.3-3
- KYOS-T: add some translation

* Wed Mar 01 2023 wangxiaoqing <wangxiaoqing@kylinsec.com.cn> - 0.0.3-2
- KYOS-B: Modify the log information level of zlog initialization failure to debug.

* Thu Oct 27 2022 wangxiaoqing <wangxiaoqing@kylinsec.com.cn> - 0.0.3-1
- KYOS-B: Do not exit when zlog init failed.

* Wed Aug 10 2022 luoqing <luoqing@kylinsec.com.cn> - 0.0.2-2.kb4
- KYOS-F: Modify license and add yaml file.

* Tue Jan 25 2022 longcheng <longcheng@kylinos.com.cn> - 0.0.2-2.kb3
- KYOS-B: remove BuildRequires: fingerprint-sdk-devel in openeuler 
- KYOS-F：Add the enable zlog ex macro for zlog.
- KYOS-B: Add BuildRequires: gcc-c++ in openeuler

* Mon Jan 24 2022 wxq <wangxiaoqing@kylinos.com.cn> - 0.0.2-2.kb2
- KYOS-F： Add the Mulan license.

* Wed Dec 29 2021 kpkg <kpkg@kylinos.com.cn> - 0.0.2-2.kb1
- rebuild for KY3.4-MATE-modules-dev

* Wed Dec 29 2021 caoyuanji<caoyuanji@kylinos.com.cn> - 0.0.2-2
- Upgrade version number for easy upgrade

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 0.0.2-1.kb3
- rebuild for KY3.4-4-KiranUI-2.2

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 0.0.2-1.kb2
- rebuild for KY3.4-4-KiranUI-2.2

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 0.0.2-1.kb1
- rebuild for KY3.4-4-KiranUI-2.2

* Mon Aug 23 2021 wxq <wangxiaoqing@kylinos.com.cn> - 0.0.2-1
- KYOS-F: update to 0.0.2 (#35700)

* Sat Mar 27 2021 wxq <wangxiaoqing@kylinos.com.cn> - 0.0.1
- KYOS-F: add the face and fprint auth.(#35700)
